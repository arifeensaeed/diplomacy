#!/usr/bin/env python3

# ---------------------------
# projects/diplomacy/Diplomacy.py
# ---------------------------

# ------------
# diplomacy_read
# ------------

def diplomacy_read(s):
    """
    read input line
    s a string
    return a list of string elements, in either of the three forms:
    [Army, Location, Hold]
    [Army, Location, Support, Army]
    [Army, Location, Move, Location]
    """
    a = s.split()
    return a

# -------------
# diplomacy_print
# -------------

def diplomacy_print(w, x, y):
    """
    print two string elements
    w a writer
    x name of army
    y name of location OR [dead]
    """
    w.write(str(x) + " " + str(y) + '\n')

# -------------
# diplomacy_solve
# -------------

def diplomacy_solve(r, w):
    """
    r a reader
    w a writer
    outputs sorted string lines of armies and locations/[dead] based on all Holds, Supports, Moves of round
    holistically calculates winners and losers for entire round
    """
    try:
        # (0) make list of lines
        master_lst = []
        for s in r:
            d_lst = diplomacy_read(s)
            master_lst.append(d_lst)

        assert len(master_lst) >= 0 # pre-conditions + argument validity

        # (1) make dict of locations
        locations_dict = {}
        for d in master_lst:
            locations_dict[d[1]] = 0
            if (d[2] == 'Move'):
                locations_dict[d[3]] = 0

        assert len(locations_dict) > 0 # pre-conditons + argument validity

        # (2) populate dict with armies for each location based on actions
        for d in master_lst:

            if d[2] == "Hold":
                if locations_dict[d[1]] == 0: # if first loop through location
                    # 2D Array
                    # [[army, support_score], [army, support_score]...]
                    locations_dict[d[1]] = [[d[0], 0]]  

                elif locations_dict[d[1]] != 0:
                    locations_dict[d[1]].append([d[0], 0])

            elif d[2] == "Move":
                if locations_dict[d[3]] == 0: # if first loop through location
                    locations_dict[d[3]] = [[d[0], 0]]
                
                elif locations_dict[d[3]] != 0:
                    locations_dict[d[3]].append([d[0], 0])

            elif d[2] == "Support":
                if locations_dict[d[1]] == 0: # if first loop through location
                    locations_dict[d[1]] = [[d[0], 0]]

                elif locations_dict[d[1]] != 0:
                    locations_dict[d[1]].append([d[0], 0])

            # else if invalid action simple ignore location

        # (3) properly adding support_score(s)
        for d in master_lst:
            if d[2] == "Support":
                for l in locations_dict:  # for every location
                    if locations_dict[l] != 0:  # if the location actually has armies in it
                        for a in locations_dict[l]:  # for every [Army, support_score]

                            # if the Army of the current [Army, support_score] equals the current line
                            # AND the supporting army can validly support
                            # because its location is not under attack
                            if (a[0] == d[3]) and (len(locations_dict[d[1]]) == 1):  
                                
                                assert a[1] >= 0 # pre-conditions validity

                                a[1] += 1 # increase support_score by 1
                     
        # (4) calculate winners and losers

        # find max support score and max army in each location
        sorted_army_lst = [] 
        for l in locations_dict:
            if locations_dict[l] != 0:
                max_support_score = 0

                # get max_support_score
                for a in locations_dict[l]:
                    if a[1] > max_support_score:
                        max_support_score = a[1]
                
                # no winners, >1 armies in location, all dead
                if (max_support_score == 0) and (len(locations_dict[l]) > 1):
                    
                    for a in locations_dict[l]:
                        sorted_army_lst.append([a[0], "[dead]"])

                # only 1 army in location, remains alive, regardless of support_score
                elif (max_support_score >= 0) and (len(locations_dict[l]) == 1):

                    sorted_army_lst.append([a[0], l])
                    
                # 2 scenarios, presented below:
                elif (max_support_score > 0) and (len(locations_dict[l]) > 1):

                    # get max_army_count
                    max_army_count = 0
                    for a in locations_dict[l]:
                        if a[1] == max_support_score:
                            max_army_count += 1

                    # a) one clear winner in the location, all else dead
                    if max_army_count == 1:
                        for a in locations_dict[l]:
                            if (a[1] == max_support_score):  # winner of location
                                sorted_army_lst.append([a[0], l])
                            elif a[1] < max_support_score:  # loser of location
                                sorted_army_lst.append([a[0], "[dead]"])

                    # b) all armies dead, no one army has the most support even if individual support_scores are > 1
                    else:
                        for a in locations_dict[l]:
                            sorted_army_lst.append([a[0], "[dead]"])

        # sort final list and print
        assert len(sorted_army_lst) > 0 # post-conditions validity

        sorted_army_lst.sort()
        for i in sorted_army_lst:
            diplomacy_print(w, i[0], i[1])
    
    except: # no input passed in
        pass