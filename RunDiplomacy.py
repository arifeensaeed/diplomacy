#!/usr/bin/env python3

# ------------------------------
# projects/diplomacy/RunDiplomacy.py
# ------------------------------

# -------
# imports
# -------

import sys

from Diplomacy import diplomacy_solve

# ----
# main
# ----

if __name__ == "__main__":
    diplomacy_solve(sys.stdin, sys.stdout)

""" #pragma: no cover
$ cat RunDiplomacy.in


$ python RunDiplomacy.py < RunDiplomacy.in > RunDiplomacy.out

$ cat RunDiplomacy.out

$ python -m pydoc -w Diplomacy"
# That creates the file Diplomacy.html
"""
